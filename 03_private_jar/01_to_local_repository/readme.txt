文档说明: 定制库到Maven本地仓库

01. 使用场景
这里有2种场景，需要使用Maven的手动命令将指定 jar 定制到 Maven 的本地资源库。
a). 要使用的 jar 不存在于 Maven 的中央储存库中。 
b). 您创建了一个自定义的 jar ，而另一个 Maven 项目需要使用。 
PS，还是有很多 jar 不支持 Maven 的。 

02. 案例学习
eg: kaptcha，它是一个流行的第三方Java库，它被用来生成 “验证码” 的图片，以阻止垃圾邮件，但它不在 Maven 的中央仓库中。 
在本教程中，我们将告诉你如何安装 kaptcha.jar 到 Maven 的本地资源库。

A. mvn安装
下载 "kaptcha"，将其解压缩, 并将 kaptcha-version.jar 复制到其他地方，比如：C盘。
执行命令：D:\>mvn install:install-file -Dfile=c:\kaptcha-2.3.jar -DgroupId=com.google.code -DartifactId=kaptcha -Dversion=2.3 -Dpackaging=jar
执行结果如下: 
[INFO] Scanning for projects...
[INFO] Searching repository for plugin with prefix: 'install'.
[INFO] ------------------------------------------------------------------------
[INFO] Building Maven Default Project
[INFO]    task-segment: [install:install-file] (aggregator-style)
[INFO] ------------------------------------------------------------------------
[INFO] [install:install-file]
[INFO] Installing c:\kaptcha-2.3.jar to D:\maven_repo\com\google\code\kaptcha\2.3\kaptcha-2.3.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESSFUL
[INFO] ------------------------------------------------------------------------
[INFO] Total time: < 1 second
[INFO] Finished at: Tue May 12 13:41:42 SGT 2014
[INFO] Final Memory: 3M/6M
[INFO] ------------------------------------------------------------------------

B. pom.xml使用
现在，“kaptcha.jar” 已被复制到 Maven 本地存储库。可以在 pom.xml 中声明 kaptcha 的坐标。
<dependency>
    <groupId>com.google.code</groupId>
    <artifactId>kaptcha</artifactId>
    <version>2.3</version>
</dependency> 

C. 完成
完成构建jar，现在 “kaptcha.jar” 能够从 Maven 的本地存储库检索了。

附1: 参考
A) Maven安装文档
B) Kaptcha网站

附2: 
自测命令, eg: 
mvn install:install-file -Dfile=E:\task\software\develop\repertory\studynotes\tech\maven\20_to_local_repository\kaptcha-2.3.2\kaptcha-2.3.2.jar -DgroupId=com.google.code -DartifactId=kaptcha -Dversion=2.3.2 -Dpackaging=jar


参照云笔记: [C]-Maven私包依赖-[A]-定制库到Maven本地仓库
http://note.youdao.com/noteshare?id=67262c692149f2341cbdcdf2eff7e804&sub=4F3D1687B187465CB9A55BF8DDC1CBA3