```
title: SHORTCUT KEYS
```
# Maven

[![img](http://maven.apache.org/images/maven-logo-black-on-white.png)](http://maven.apache.org/)

![Maven](http://maven.apache.org/images/apache-maven-project.png)

## 重点内容

### [Maven安装配置](https://www.yiibai.com/maven/maven-local-repository.html)

### [Maven本地仓库](https://www.yiibai.com/maven/maven-local-repository.html)

### [Maven插件](https://www.yiibai.com/maven/maven_plugins.html)

### [Maven构建生命周期](https://www.yiibai.com/maven/maven_build_life_cycle.html)

### [Maven私服](https://help.sonatype.com/repomanager2)

## 参考

[Maven官方文档](http://maven.apache.org/users/index.html)

[Maven官方教程](http://maven.apache.org/guides/getting-started/index.html)

[易百Maven教程](https://www.yiibai.com/maven)

