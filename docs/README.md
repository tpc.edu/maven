```
title: SHORTCUT KEYS
```
# Maven

[![img](http://maven.apache.org/images/maven-logo-black-on-white.png)](http://maven.apache.org/)

![Maven](http://maven.apache.org/images/apache-maven-project.png)

## 重点内容

1. Maven安装配置

02. Maven仓库
      A. [Maven本地仓库](repository/[B]-Maven仓库.pdf)
      B. Maven中央仓库
      C. Maven远程仓库
      D. Maven私服
    
03. Maven私包依赖
      A. [Maven外部依赖](dependency/[C]-Maven私包依赖.pdf)
      B. [Maven定制库到Maven本地仓库](dependency/[C]-Maven私包依赖-[A]-定制库到Maven本地仓库.pdf)
    
4. Maven配置文件

     A. [Maven配置文件](profile/[D]-Maven配置文件.pdf)

5. 自动化(构建/部署)
     A. [Maven构建自动化](automation/[E]-Maven自动化-[A]-Maven构建自动化.md)
     B. [Maven自动化部署](automation/[E]-Maven自动化-[B]-Maven自动化部署.md)
     C. [部署war至Tomcat](automation/[E]-Maven自动化-[C]-部署war至Tomcat.md)

6. 其他
     A. Maven插件
     B. [Maven快照](other/[F]-其他-[B]-Maven快照.pdf)
     C. Maven依赖机制
     D. [Maven生命周期](other/[F]-其他-[D]-Maven生命周期.pdf)
     E. [Maven代理访问](other/[F]-其他-[E]-Maven代理访问.pdf)
     F. Maven项目文档
     G. Maven项目模板

## 参考

[Maven官方网站](http://maven.apache.org/)

[Maven官方文档](http://maven.apache.org/users/index.html)

[Maven官方教程](http://maven.apache.org/guides/getting-started/index.html)

[易百Maven教程](https://www.yiibai.com/maven)

