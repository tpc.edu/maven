[TOC]

# Maven部署war至Tomcat

## 概述
为了减轻开发人员负担及避免手动部署失误，Tomcat提供了打包并部署war包至Tomcat的Maven插件。

### 问题描述及使用场景
* 场景: 部署新功能上线时，需要先打包生成war，再手动将其放置Tomcat的相关目录下并启动。  
* 问题: 手动操作就有可能会存在误操，且需要耗费相关人力。那么是否有相关Maven插件可以实现自动打包并部署war至Tomcat的功能呢？  
* 解决: 通过学习Tomcat6[^1]及Tomcat7[^2]提供的`Manager App HOW-TO`发现Tomcat[^3]开发了自动打包及部署war至Tomcat的Maven plugin, 并提供了相应的[Maven命令](http://tomcat.apache.org/maven-plugin-trunk/context-goals.html)[^4]. 

### 简要实现
* 基础环境
  1. JDK8
  2. Maven 3
  3. Tomcat 6.0.37/Tomcat 7.0.53
* Tomcat 6
```
## 发布URL: (http://localhost:8080/manager/)
<protocol>://<domain name>:<port>/manager/
## 命令:
mvn tomcat6:deploy
```
* Tomcat 7
```
## 发布URL: (http://localhost:8080/manager/text)
<protocol>://<domain name>:<port>/manager/text
## 命令:
mvn tomcat7:deploy
```

## Tomcat 7 示例
这个例子说明了如何在Tomcat7打包并部署WAR文件。
### Tomcat认证
在`%TOMCAT7_PATH%/conf/tomcat-users.xml`文件中, 添加具有角色管理器GUI和管理脚本的用户。
```
<?xml version='1.0' encoding='utf-8'?>
<tomcat-users>
  <role rolename="manager-gui"/>
  <role rolename="manager-script"/>
  <user username="admin" password="password" roles="manager-gui,manager-script" />
</tomcat-users>
```
### Maven认证
在`%MAVEN_PATH%/conf/settings.xml`文件中, 添加上面的Tomcat 用户，Maven使用该用户来登录Tomcat服务器。
```
<?xml version="1.0" encoding="UTF-8"?>
<settings ...>
  <servers>
    <server>
      <id>TomcatServer</id>
      <username>admin</username>
      <password>password</password>
    </server>
  </servers>
</settings>
```
### Tomcat7 Maven 插件
在`pom.xml`文件中, 声明一个Maven的Tomcat插件。
```
<plugin>
  <groupId>org.apache.tomcat.maven</groupId>
  <artifactId>tomcat7-maven-plugin</artifactId>
  <version>2.2</version>
  <configuration>
    <url>http://localhost:8080/manager/text</url>
    <server>TomcatServer</server>
    <path>/yiibaiWebApp</path>
  </configuration>
</plugin>
```
**==怎么运行的==**  
Maven使用`settings.xml`中`TomcatServer`对应的用户名及密码进行Tomcat的认证, 验证通过后Maven通过`http://localhost:8080/manager/text`将war文件部署至同域名Tomcat服务器`path`为`/yiibaiWebApp`的`webapps`路径下.
### 发布到Tomcat
操作Tomcat WAR文件的命令如下: 
```
mvn tomcat7:deploy 
mvn tomcat7:undeploy 
mvn tomcat7:redeploy
```
#### 示例
```
> mvn tomcat7:deploy

...
[INFO] Deploying war to http://localhost:8080/yiibaiWebApp
Uploading: http://localhost:8080/manager/text/deploy?path=%2FyiibaiWebApp&update=true
Uploaded: http://localhost:8080/manager/text/deploy?path=%2FyiibaiWebApp&update=true (13925 KB at 35250.9 KB/sec)

[INFO] tomcatManager status code:200, ReasonPhrase:OK
[INFO] OK - Deployed application at context path /yiibaiWebApp
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 8.507 s
[INFO] Finished at: 2015-08-05T11:35:25+08:00
[INFO] Final Memory: 28M/308M
[INFO] ------------------------------------------------------------------------
```

## Tomcat 6 示例
这个例子说明了如何在Tomcat6打包和部署WAR文件。这些步骤和Tomcat7是一样的，只是部署URL和命令名称不同。
### Tomcat认证
在`%TOMCAT6_PATH%/conf/tomcat-users.xml`文件中, 添加具有角色管理器GUI和管理脚本的用户。
```
<?xml version='1.0' encoding='utf-8'?>
<tomcat-users>
  <role rolename="manager-gui"/>
  <role rolename="manager-script"/>
  <user username="admin" password="password" roles="manager-gui,manager-script" />
</tomcat-users>
```
### Maven认证
在`%MAVEN_PATH%/conf/settings.xml`文件中, 添加上面的Tomcat 用户，Maven使用该用户来登录Tomcat服务器。
```
<?xml version="1.0" encoding="UTF-8"?>
<settings ...>
  <servers>
    <server>
      <id>TomcatServer</id>
      <username>admin</username>
      <password>password</password>
    </server>
  </servers>
</settings>
```
### Tomcat7 Maven 插件
在`pom.xml`文件中, 声明一个Maven的Tomcat插件。
```
<plugin>
  <groupId>org.apache.tomcat.maven</groupId>
  <artifactId>tomcat6-maven-plugin</artifactId>
  <version>2.2</version>
  <configuration>
    <url>http://localhost:8080/manager</url>
    <server>TomcatServer</server>
    <path>/yiibaiWebApp</path>
  </configuration>
</plugin>
```
**==怎么运行的==**  
Maven使用`settings.xml`中`TomcatServer`对应的用户名及密码进行Tomcat的认证, 验证通过后Maven通过`http://localhost:8080/manager`将war文件部署至同域名Tomcat服务器`path`为`/yiibaiWebApp`的`webapps`路径下.
### 发布到Tomcat
操作Tomcat WAR文件的命令如下: 
```
mvn tomcat6:deploy 
mvn tomcat6:undeploy 
mvn tomcat6:redeploy
```
#### 示例
```
> mvn tomcat6:deploy

...
[INFO] Deploying war to http://localhost:8080/yiibaiWebApp
Uploading: http://localhost:8080/manager/deploy?path=%2FyiibaiWebApp
Uploaded: http://localhost:8080/manager/deploy?path=%2FyiibaiWebApp (13925 KB at 32995.5 KB/sec)

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 22.652 s
[INFO] Finished at: 2014-08-05T12:18:54+08:00
[INFO] Final Memory: 30M/308M
[INFO] ------------------------------------------------------------------------
```

## 疑问
该技术目前已经有些过时了, 已被当下以`Kubernetes (K8s)`为核心的`DevOps`云技术所替代, 后者的自动部署/日志分析/性能检测等功能更加有优势.

[^1]: [Apache Tomcat 6 Manager App HOW-TO](http://tomcat.apache.org/tomcat-7.0-doc/manager-howto.html)
[^2]: [Apache Tomcat 7 Manager App HOW-TO](http://tomcat.apache.org/tomcat-6.0-doc/manager-howto.html)
[^3]: [Tomcat Maven Plugin](http://tomcat.apache.org/maven-plugin.html)
[^4]: [Tomcat Maven Plugin – Context Goals](http://tomcat.apache.org/maven-plugin-trunk/context-goals.html)