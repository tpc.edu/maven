[TOC]

# Maven自动化部署

## 概述
在项目开发中，部署过程通常包含以下步骤:
1. 检查在建项目代码全部进入SVN或Git库中，并标记它。
2. 从SVN/Git下载完整的源代码。
3. 构建应用程序。
4. 将生成的WAR或JAR文件存储到指定的网络位置。
5. 从该网络位置获取文件并部署到生产现场。
5. 更新应用程序的日期和版本号。

### 问题描述及使用场景
* 场景: 通常有多人参与了上述部署过程，一个团队手动签入代码，其他人处理构建等。 由于涉及多团队手动操作，任何一个步骤出错都会导致整个部署过程出问题。例如，网络设备中较旧的版本可能不会被替换，从而导致部署团队部署旧版本。 
* 问题: 那么是否能实现自动化部署呢？  
* 解决: 使用自动化部署后的部署过程如下: 
  1. Maven构建和释放项目;
  2. SVN源代码库管理源代码;
  3. 远程存储库管理器(Jfrog/Nexus)管理项目的二进制文件。

## 使用`maven-plugin`实现自动化部署
### pom.xml
使用Maven的发布插件来创建一个自动释放过程, 
例如：`bus-core-api`项目pom.xml
```
<project xmlns="http://maven.apache.org/POM/4.0.0" 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>bus-core-api</groupId>
  <artifactId>bus-core-api</artifactId>
  <version>1.0-SNAPSHOT</version>
  <packaging>jar</packaging> 
  <scm>
    <url>http://www.svn.com</url>
    <connection>scm:svn:http://localhost:8080/svn/jrepo/trunk/Framework</connection>
    <developerConnection>scm:svn:${username}/${password}@localhost:8080:common_core_api:1101:code</developerConnection>
  </scm>
  <distributionManagement>
    <repository>
      <id>Core-API-Java-Release</id>
      <name>Release repository</name>
      <url>http://localhost:8081/nexus/content/repositories/Core-Api-Release</url>
    </repository>
  </distributionManagement>
  <build>
    <plugins>
      <plugin>
       <groupId>org.apache.maven.plugins</groupId>
       <artifactId>maven-release-plugin</artifactId>
       <version>2.0-beta-9</version>
       <configuration>
         <useReleaseProfile>false</useReleaseProfile>
         <goals>deploy</goals>
         <scmCommentPrefix>[bus-core-api-release-checkin]-</scmCommentPrefix>
       </configuration>
      </plugin>
    </plugins>
  </build>
</project>
```
在pom.xml中，使用的重要元素如下:
| 元素         | 描述                                                         |
| ------------ | ------------------------------------------------------------ |
| scm          | Configures the SVN location from where Maven will check out the source code. |
| repositories | Location where built WAR/EAR/JAR or any other artifact will be stored after code build is successful. |
| plugin       | maven-release-plugin is configured to automate the deployment process. |

### Maven插件发布命令
Maven`maven-release-plugin`常用的任务如下:  
1. `mvn release:clean`  
   
   > 清洁,防止工作区最近一次的释放过程并不顺利。
2. `mvn release:rollback`  
   
   > 回滚,防止工作空间代码和配置更改的最近一次的释放过程并不顺利。
3. `mvn release:prepare`  
   > 执行多个操作: 
   > - 检查是否有未提交的本地更改
   > - 确保没有最新的快照依赖
   > - 更改应用程序的版本并删除旧快照版本
   > - 更新文件到SVN
   > - 运行测试用例
   > - 提交修改后POM文件
   > - 在subversion中标签代码
   > - 增加版本号和附加快照为后续发版做准备
   > - 提交修改后的POM文件到SVN

4. `mvn release:perform`
   
   > 检出使用前面定义的标签代码, 并运行Maven的把目标WAR部署到Nexus。

### 示例:
打开控制台, 进入`C: > MVN > bus-core-api`目录执行以下mvn命令:
```
C:MVN\bus-core-api>mvn release:prepare
```
Maven将开始建设该项目, 构建成功后运行下面的mvn命令: 
```
C:MVN\bus-core-api>mvn release:perform
```
构建成功后，可以在Nexus验证最新的JAR文件是否上传。

## 疑问
### Maven私服学习(Nexus)
### 该技术目前已经有些过时了, 已被当下以`Kubernetes (K8s)`为核心的`DevOps`云技术所替代, 后者的自动部署/日志分析/性能检测等功能更加有优势.