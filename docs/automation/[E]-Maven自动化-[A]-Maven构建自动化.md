[TOC]

# Maven构建自动化

## 概述
### 构建自动化定义
在相关工程项目构建过程中, 被依赖的项目构建完成后, 将自动触发其上层依赖项目的构建工作,  以确保所依赖项目的稳定性.

### 问题描述及使用场景
依赖: 依赖API项目`bus-core-api`的两个上层应用: 网页UI项目`app-web-ui`和桌面UI`app-desktop-ui`;  
场景: 每当`bus-core-api:1.0-SNAPSHOT`发生变化时, `app-web-ui`和`app-desktop-ui`都需要手动执行构建才能获取最新的`bus-core-api:1.0-SNAPSHOT`;  
问题: 是否能在`bus-core-api:1.0-SNAPSHOT`发生变化时自动触发`app-web-ui`和`app-desktop-ui`的构建呢?  

`api`使用快照可以确保`ui`项目使用的是最新的`api`, 但要做到成功构建`api`后自动触发`ui`的构建有下面两种方式:
* 使用`maven-plugin`实现自动化构建
* 使用持续集成（CI）服务器的自动管理功能实现构建自动化

## 使用`maven-plugin`实现自动化构建
`app-web-ui`项目依赖`bus-core-api:1.0-SNAPSHOT1.0-SNAPSHOT`
```
<project xmlns="http://maven.apache.org/POM/4.0.0" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>app-web-ui</groupId>
  <artifactId>app-web-ui</artifactId>
  <version>1.0</version>
  <packaging>jar</packaging>
  <name>app-web-ui</name>
  <url>http://maven.apache.org</url>
  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>bus-core-api</groupId>
      <artifactId>bus-core-api</artifactId>
      <version>1.0-SNAPSHOT</version>
      <scope>system</scope>
      <systemPath>C:\MVN\bus-core-api\target\bus-core-api-1.0-SNAPSHOT.jar</systemPath>
    </dependency>
  </dependencies>
</project>
```

`app-desktop-ui`项目依赖`bus-core-api:1.0-SNAPSHOT1.0-SNAPSHOT`
```
<project xmlns="http://maven.apache.org/POM/4.0.0" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"> 
  <groupId>app-desktop-ui</groupId>
  <artifactId>app-desktop-ui</artifactId>
  <version>1.0</version>
  <packaging>jar</packaging>
  <dependencies>
    <dependency>
      <groupId>bus-core-api</groupId>
      <artifactId>bus-core-api</artifactId>
      <version>1.0-SNAPSHOT</version>
    </dependency>
  </dependencies>
</project>
```

`bus-core-api:1.0-SNAPSHOT1.0-SNAPSHOT`
```
<project xmlns="http://maven.apache.org/POM/4.0.0" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"> 
  <groupId>bus-core-api</groupId>
  <artifactId>bus-core-api</artifactId>
  <version>1.0-SNAPSHOT</version>
  <packaging>jar</packaging>
  <build>
    <plugins>
      <plugin>
        <artifactId>maven-invoker-plugin</artifactId>
        <version>1.6</version>
        <configuration>
          <debug>true</debug>
          <pomIncludes>
            <pomInclude>app-web-ui/pom.xml</pomInclude>
            <pomInclude>app-desktop-ui/pom.xml</pomInclude>
          </pomIncludes>
        </configuration>
        <executions>
          <execution>
            <id>build</id>
            <goals>
              <goal>run</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
</project>
```
1. 打开控制台, 在目录`C:\MVN\bus-core-api`下执行下面`mvm`命令:
```
C:\MVN\bus-core-api> mvn clean package -U
```
2. Maven开始构建项目`bus-core-api`
```
[INFO] Scanning for projects...
[INFO] ----------------------------------------------------
[INFO] Building bus-core-api
[INFO]    task-segment: [clean, package]
[INFO] ----------------------------------------------------
...
[INFO] [jar:jar {execution: default-jar}]
[INFO] Building jar: C:MVN us-core-api arget
bus-core-api-1.0-SNAPSHOT.jar
[INFO] ----------------------------------------------------
[INFO] BUILD SUCCESSFUL
[INFO] ----------------------------------------------------
```
3. 当`bus-core-api`构建成功后, Maven开始构建`app-web-ui`
```
[INFO] ----------------------------------------------------
[INFO] Building app-web-ui 
[INFO]    task-segment: [package]
[INFO] ----------------------------------------------------
...
[INFO] [jar:jar {execution: default-jar}]
[INFO] Building jar: C:MVNapp-web-ui arget
app-web-ui-1.0-SNAPSHOT.jar
[INFO] ----------------------------------------------------
[INFO] BUILD SUCCESSFUL
[INFO] ----------------------------------------------------
```
4. 当`app-web-ui`构建成功后, Maven开始构建`app-desktop-ui`
```
[INFO] ----------------------------------------------------
[INFO] Building app-desktop-ui 
[INFO]    task-segment: [package]
[INFO] ----------------------------------------------------
...
[INFO] [jar:jar {execution: default-jar}]
[INFO] Building jar: C:/MVN/app-desktop-ui arget
app-desktop-ui-1.0-SNAPSHOT.jar
[INFO] ----------------------------------------------------
[INFO] BUILD SUCCESSFUL
[INFO] ----------------------------------------------------
```

## 使用持续集成（CI）服务实现构建自动化
当`bus-core-api`源代码SVN更新后, Hudson开始`api`的构建;  当`gui`构建成功后, Hudson自动查找相关的项目, 并开始构建`app-web-ui`和`app-desktop-ui`项目。  
![Hudson](https://www.yiibai.com/uploads/allimg/131227/0I10S147-0.jpg "使用GUI工具实现构建自动化")  

## 疑问
### 多工作区该自动化是否起作用?
`api`和`ui`项目在同一台开发机器上时, 该自动化是有效的. 那么, 如果`api`和`ui`分别在不同开发机器上时, 该自动化还是否有效?
### 向下激活是否有意义? 是否存在向下激活?
`ui`项目在构建时, 自动构建`api`是否有存在的必要, 是否有意义或价值? 如果有, 那么是否有该自动化实现方式?