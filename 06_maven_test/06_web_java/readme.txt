-- 使用Maven创建Java项目
01. 使用Maven命令创建Java项目
02. Maven目录布局
	project_java_name
	|-src
	|--main
	|---java
	|----com.group
	|-----App.java
	|--test
	|---java
	|----com.group
	|-----AppTest.java
	|-pom.xml
03. 整合eclipse(忽略)
04. Maven打包
05. 运行

## step_01. 使用Maven命令创建Java项目
## 注意: 执行命令的目录下尽量不要有pom.xml文件
cd E:\task\software\develop\repertory\studynotes\tech\maven\80_maven_test\02_web_java
mvn archetype:generate -DgroupId=com.baidu -DartifactId=java -Dversion=1.0-SNAPSHOT -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false

## step_02. 打包
cd java
mvn package

## step_03. 运行App.java
java -cp target/java-1.0-SNAPSHOT.jar com.baidu.App


++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- 使用Mave创建Web应用程序项目
01. 使用Maven命令创建Web项目
02. Maven目录布局
	project_web_name
	|-src
	|--main
	|---java
	|----com.group
	|-----App.java
	|---resource
	|---webapp
	|----WEB-INF
	|-----web.xml
	|----index.jsp
	|--test
	|---java
	|----com.group
	|-----AppTest.java
	|---resource
	|-pom.xml

## step_01. 使用Maven命令创建Web项目
cd E:\task\software\develop\repertory\studynotes\tech\maven\80_maven_test\02_web_java
mvn archetype:generate -DgroupId=com.baidu -DartifactId=web -Dversion=1.0-SNAPSHOT -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false

## step_02. 打包
cd web
mvn package