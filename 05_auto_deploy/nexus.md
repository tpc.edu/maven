[TOC]
# Maven 使用 nexus 私服

## nexus配置

### 创建nexus仓库

- Nexus Repository Manager 3 [Repository Management](https://help.sonatype.com/repomanager3/repository-management)
- Nexus Repository Manager 2 [Managing Repositories](https://help.sonatype.com/repomanager2/configuration/managing-repositories)

### 创建nexus用户并授权

- Nexus Repository Manager 3 [User Authentication](https://help.sonatype.com/repomanager3/system-configuration/user-authentication)
- Nexus Repository Manager 2 [Managing Security](https://help.sonatype.com/repomanager2/configuration/managing-security)


## Maven使用nexus私服配置

### Maven使用nexus上传下载jar
修改Maven的settings文件:
```xml
<settings>
  <mirrors>
    <mirror>
      <!--This sends everything else to /public -->
      <id>nexus</id>
      <mirrorOf>*</mirrorOf>
      <url>http://localhost:8081/nexus/content/groups/public</url>
    </mirror>
  </mirrors>
  <profiles>
    <profile>
      <id>nexus</id>
      <!--Enable snapshots for the built in central repo to direct -->
      <!--all requests to nexus via the mirror -->
      <repositories>
        <repository>
          <id>central</id>
          <url>http://central</url>
          <releases><enabled>true</enabled></releases>
          <snapshots><enabled>true</enabled></snapshots>
        </repository>
      </repositories>
     <pluginRepositories>
        <pluginRepository>
          <id>central</id>
          <url>http://central</url>
          <releases><enabled>true</enabled></releases>
          <snapshots><enabled>true</enabled></snapshots>
        </pluginRepository>
      </pluginRepositories>
    </profile>
  </profiles>
  <activeProfiles>
    <!--make the profile active all the time -->
    <activeProfile>nexus</activeProfile>
  </activeProfiles>
</settings>
```
> [**Guide to Large Scale Centralized Deployments**](https://maven.apache.org/guides/mini/guide-large-scale-centralized-deployments.html "集中式大规模部署指南")
> [Nexus Apache Maven](https://help.sonatype.com/repomanager2/maven-and-other-build-tools/apache-maven "Nexus在Maven中使用")
> [Setting up Multiple Repositories](https://maven.apache.org/guides/mini/guide-multiple-repositories.html "repositories")
> [Settings Reference](https://maven.apache.org/settings.html "servers repositories")

### Maven配置nexus用户信息

```xml
<!--配置上传权限-->
<server>
    <!--这是仓库的ID-->
    <id>depart2</id>
    <!--这是用户名-->
    <username>depart2-user</username>
    <!--这是密码-->
    <password>123</password>
</server>
```

### 项目 pom.xml 的配置
在要上传工程的`pom.xml`文件中加入下面配置：
```xml
<distributionManagement>
	<repository>
		<id>depart2</id>
		<name>depart2</name>
		<url>http://localhost:8081/nexus/content/repositories/depart2/</url>
	</repository>
</distributionManagement>
```

在项目中执行`mvn deploy`即可把该工程的jar上传至nexus私服中.
> [Security and Deployment Settings](https://maven.apache.org/guides/mini/guide-deployment-security-settings.html "Maven services 配置")

## 参考

- [Maven私服搭建](https://my.oschina.net/littlestyle/blog/833732)
- [Maven私服](https://my.oschina.net/littlestyle/blog/833732)
