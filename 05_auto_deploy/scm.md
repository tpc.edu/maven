[SCM Usage](http://maven.apache.org/scm/maven-scm-plugin/usage.html)

SCM:
OVERVIEW
 Introduction
 Goals
 Usage
EXAMPLES
 Bootstrapping a Project Using a POM
 Other SCM Commands
QUICK LINKS
 SCM URL Format