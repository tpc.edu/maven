Maven自动化部署
从 版本服务器 拉取最新代码并提交至 文件服务器(确保本地文件已提交)
步骤一: 从 版本服务器 拉取代码
步骤二: 修改pom.xml文件, 加入scm及release

准备工作:
	svn(git), Maven-plugin(scm,release), maven私服(nexus)

插件使用说明:
01. 自动部署之scm插件
	maven的scm插件使用: http://maven.apache.org/scm/maven-scm-plugin/usage.html
	常用命令
		mvn scm:checkin -Dmessage="代码提交日志" #代码提交
		mvn scm:update #代码更新
02. 自动部署之release插件
	maven的release插件使用: http://maven.apache.org/maven-release/maven-release-plugin/usage.html
	常用命令
		mvn release:clean #发布前的清理
		mvn release:prepare #发布版本至svn版本管理库(pom.xml版本号更新并提交至svn, 本地package, svn服务端打包(服务器端的最新版本, 但本地不会自动更新))
		mvn release:prepare --batch-mode #发布分支
		mvn release:prepare -DdryRun=true #排练(不做任何动作, 只是预演)
		mvn release:rollback #回滚版本
		mvn release:perform #

附录: 
Maven发布插件
Maven使用确实下列有用的任务maven-release-plugin.
	mvn release:clean
		它清除以防工作区的最后一个释放的过程并不顺利。
	
	mvn release:rollback
		回滚是为了以防工作空间代码和配置更改的最后一个释放的过程并不顺利。

	mvn release:prepare
		执行多个操作次数
		检查是否有任何未提交的本地更改或不
		确保没有快照依赖
		更改应用程序的版本并删除快照从版本，以释放
		更新文件到 SVN.
		运行测试用例
		提交修改后POM文件
		标签代码在subversion中
		增加版本号和附加快照以备将来发行
		提交修改后的POM文件到SVN。

	mvn release:perform
		检查出使用前面定义的标签代码并运行Maven的部署目标来部署战争或内置工件档案库。

注意:
	01. 如果需要跳过单元测试，可以加入参数 -Darguments="-DskipTests"，直接使用-Dmaven.test.skip=true是无效的。
	02. 在执行mvn release:perform时默认会生成api文档，如果默写注释不符合规范的话会造成构建失败，可以加参数-DuseReleaseProfile=false取消构建api文档，或则需要根据规范书写注释。

