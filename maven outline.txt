Maven 教程地址: https://www.yiibai.com/maven
Maven 教程目录:

Maven 安装配置
Maven 启用代理访问
Maven 本地资源库
Maven 中央存储库
Maven 远程仓库
Maven 依赖机制
Maven 定制库到本地资源库
Maven POM
Maven 构建生命周期
Maven 构建配置文件
Maven 存储库
Maven 插件
Maven 外部依赖
Maven 项目文档
Maven 项目模板
Maven 快照
Maven 构建自动化
Maven 依赖管理
Maven 自动化部署
Maven 部署war至Tomcat
Maven 创建java项目
Maven 创建Web项目

maven安装
	1. 安装JDK, 并配置"JAVA_HOME"环境变量, 测试 java -version
	2. 解压apache-maven-x.x.x-bin.zip到安装目录
	3. 配置maven的"MAVEN_HOME"环境变量, 测试 mvn -version
	4. 修改配置文件settings.xml; a. 设置本地仓库; b. 设置中央仓库为百度的maven仓库;

maven生命周期
	清理 clean
	校验 validate
	编译 compile
	测试 test
	构建 package
	安装 install
	文档 site
	部署 deploy

maven配置文件
	配置 开发/测试/生产环境的数据库服务器的相关参数
	使用<profiles><profile><id>dev</id><activation>...</activation></profile></profiles>去做配置文件的配置
	选用方式: 
		a. 明确使用命令从控制台输入, 例如: mvn clean test -P dev
		b. 基于maven的settings.xml设置
		c. 基于环境变量(用户/系统变量)
		d. OS设置(例如: Windows系列)
		e. 呈现/丢失的文件
	除abc外均在pom.xml的<profile><activation>...</activation></profile>中配置, 推荐使用设置默认activation + 控制台输入选择配置文件

maven资源库
	1. 本地库 local
	2. 中央库 central
	3. 远程库 remote

maven插件
	1. 构建插件(在pom.xml中的<build/>中进行配置)
	2. 报告插件(在pom.xml中的<reporting>中进行配置)

maven创建java项目
	C:MVN>mvn archetype:generate
	-DgroupId=com.companyname.bank 
	-DartifactId=consumerBanking 
	-DarchetypeArtifactId=maven-archetype-quickstart 
	-DinteractiveMode=false

maven外部依赖
	01. 在src/lib下放入java_test-1.0-SNAPSHOT.jar
	02. pom.xml的<dependencies>中配置
		<!-- 添加外部依赖 -->
		<dependency>
		  <groupId>com.companyname.bank</groupId>
		  <artifactId>java_test</artifactId>
		  <version>1.0-SNAPSHOT</version>
		  <scope>system</scope>
		  <systemPath>${basedir}\src\lib\java_test-1.0-SNAPSHOT.jar</systemPath>
		</dependency>

maven项目文档
	命令: mvn site
	位置: xx\project\target\site
	引擎: Maven会使用一个文件处理引擎: Doxia, 它将会读取多个源格式并将它们转换为通用文档模型文档 
			APT: 纯文本文档格式, 
			XDoc: Maven1.x的文档格式, 
			FML: 用于常问问题(FQA)文件, 
			XHTML: 可扩展HTML

maven项目模板
	命令: mvn archetype:generate
	Archetype是一个Maven插件, 其任务是按照其模板(共计十个)来创建一个项目结构. 

maven快照
	命令: mvn clean package -U
	快照: SNAPSHOT, 
			数据使用端每生成一个远程存储库都会检查新的快照版本;
			数据服务端发布会更新快照(data-service:1.0-SNAPSHOT 替换旧的 SNAPSHOT jar); 
	虽然在使用快照(NAPSHOT)时, Maven会自动获取最新的快照版本; 不过我们也可以使用 -U 强制 maven 命令下载最新的快照版本。

maven构建自动化
Maven自动化部署
Maven部署war至Tomcat
